package com.example.jesus.tempo.app.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import com.example.jesus.tempo.R
import com.example.jesus.tempo.app.global.State
import com.example.jesus.tempo.app.model.helper.doReplace
import com.example.jesus.tempo.app.model.helper.showError
import kotlinx.android.synthetic.main.activity_search_history.*
import kotlinx.android.synthetic.main.fragment_historic.*

class SearchHistoryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_history)

        doReplace(HistoricFragment(),this)

        btnBuscar.setOnClickListener { _ ->
            when {
                etQuery.text.isEmpty() -> showError(this, "Introduza o nome da cidade a buscar")
                else -> {
                    val detailFragment = DetailFragment()
                    doReplace(detailFragment,this)
                    State.observableQuery.onNext(etQuery.text.toString())
                }
            }
        }
    }

}
