package com.example.jesus.tempo.app.model.helper

import android.content.Context
import android.widget.Toast

fun showError(context: Context,message : String) = Toast.makeText(context,message,Toast.LENGTH_SHORT).show()