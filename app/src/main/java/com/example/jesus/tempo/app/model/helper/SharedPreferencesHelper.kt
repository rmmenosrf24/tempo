package com.example.jesus.tempo.app.model.helper

import android.content.Context

fun getSharedPreferences(ctx: Context) = ctx.getSharedPreferences("historico",Context.MODE_PRIVATE)

fun savePreference(ctx: Context,name: String) = getSharedPreferences(ctx).edit().putString(name, name).apply()

fun getAllPreferences(ctx: Context) = getSharedPreferences(ctx).all.map{x -> x.key}