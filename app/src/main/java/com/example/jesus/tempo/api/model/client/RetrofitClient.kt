package com.example.jesus.tempo.api.model.client

import retrofit2.Call
import com.example.jesus.tempo.api.model.vo.ModelGeonames
import com.example.jesus.tempo.api.model.vo.ModelWeatherObservations
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.subjects.Subject
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface RetrofitClient{

    @GET("searchJSON")
    fun listGeopoints (@Query("q") q: String,
                      @Query("maxRows") maxRows: String = "20",
                      @Query("startRow") startRow: String= "0",
                      @Query("lang") lang: String = "es",
                      @Query("isNameRequired") isNameRequired: Boolean = true,
                      @Query("style") style: String = "full",
                      @Query("username") username: String = "ilgeonamessample"
                      ): Observable<Response<ModelGeonames>>

    @GET("weatherJSON")
    fun weatherObservations(@Query("east")east: String,
                            @Query("south") south: String,
                            @Query("north") north: String,
                            @Query("west") west: String, @Query("username") username: String = "ilgeonamessample"): Observable<Response<ModelWeatherObservations>>
}