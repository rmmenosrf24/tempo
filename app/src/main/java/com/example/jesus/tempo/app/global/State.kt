package com.example.jesus.tempo.app.global


import io.reactivex.subjects.BehaviorSubject

object State {
    val observableQuery = BehaviorSubject.create<String>()
}

