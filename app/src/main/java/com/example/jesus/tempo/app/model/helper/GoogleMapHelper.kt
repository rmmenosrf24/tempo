package com.example.jesus.tempo.app.model.helper

import com.example.jesus.tempo.R
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

fun setUpMap(smap: SupportMapFragment ,p: Pair<String, String>, name: String, listLatLng: List<Pair<String, LatLng>>?) {
    smap.getMapAsync { map ->
        val latLng = LatLng(p.first.toDouble(), p.second.toDouble())
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 7f))
        map.addMarker(MarkerOptions().position(latLng).title(name))
        listLatLng?.forEach { pair ->
            val marker = map.addMarker(MarkerOptions().position(pair.second).title(pair.first))
            marker.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.ic_station_weather))
        }

    }
}