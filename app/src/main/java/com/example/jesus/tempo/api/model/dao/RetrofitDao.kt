package com.example.jesus.tempo.api.model.dao

import com.example.jesus.tempo.api.model.client.RetrofitClient
import com.example.jesus.tempo.api.model.vo.Coordenadas
import com.example.jesus.tempo.api.model.vo.ModelGeonames
import com.example.jesus.tempo.api.model.vo.ModelWeatherObservations
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

fun getApiClient() = {
    Retrofit
        .Builder()
        .baseUrl("http://api.geonames.org/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(RetrofitClient::class.java)
}



class RetrofitGeonamesDao : IDao<String,Observable<Response<ModelGeonames>>> {


    override fun getElement(query: String): Observable<Response<ModelGeonames>> = getApiClient()().listGeopoints(query)
}

class RetrofitWeatherObservationsDao : IDao<Coordenadas,Observable<Response<ModelWeatherObservations>>>{
    override fun getElement(query: Coordenadas): Observable<Response<ModelWeatherObservations>> = getApiClient()()
            .weatherObservations(north = query.north,east = query.east,west = query.west,south = query.south)
}