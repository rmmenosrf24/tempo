package com.example.jesus.tempo.api.model.facade.accions

import com.example.jesus.tempo.api.model.dao.RetrofitWeatherObservationsDao
import com.example.jesus.tempo.api.model.vo.Coordenadas
import com.example.jesus.tempo.api.model.vo.ModelWeatherObservations

object FetchWeatherObservationAccion{
    private val truncator = {x: String ->
        val arr = x.split(".")
        "${arr[0]}.${arr[1].take(1)}"
    }
    operator fun invoke(coordenadas :Coordenadas) =
            RetrofitWeatherObservationsDao()
                    .getElement(Coordenadas(truncator(coordenadas.north),truncator(coordenadas.east),truncator(coordenadas.west),truncator(coordenadas.south)))

}