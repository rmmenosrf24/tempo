package com.example.jesus.tempo.app.model.helper

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v4.app.FragmentTransaction
import com.example.jesus.tempo.R

fun fragmentTransaction(context: FragmentActivity) = context.supportFragmentManager

fun doReplace(fragment: Fragment, context: FragmentActivity) {
    val transac = fragmentTransaction(context).beginTransaction()
    transac?.replace(R.id.frContedor, fragment)
    transac?.commit()
}



