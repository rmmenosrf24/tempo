package com.example.jesus.tempo.api.model.facade

import com.example.jesus.tempo.api.model.facade.accions.FetchGeopointsAccion
import com.example.jesus.tempo.api.model.facade.accions.FetchWeatherObservationAccion
import com.example.jesus.tempo.api.model.vo.Coordenadas

object GeoWeatherFacade{
    fun fetchGeopoints(query:  String) = FetchGeopointsAccion(query)
    fun fetchWeatherObservations(coordenadas: Coordenadas) = FetchWeatherObservationAccion(coordenadas)

}