package com.example.jesus.tempo.api.model.dao


interface IDao<P,R>{

    fun getElement(query: P): R?
}