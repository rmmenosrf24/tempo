package com.example.jesus.tempo.api.model.vo

import com.google.gson.annotations.SerializedName

data class Coordenadas(val north: String, val east: String, val west: String, val south: String)

data class Bbox(val north: String, val east: String, val west: String, val south: String,val accuracy: String)

data class Geonames(@SerializedName("bbox")val bbox: Bbox, @SerializedName("name")val name: String,
                    @SerializedName("lat")val lat: String, @SerializedName("lng") val lng: String)

data class ModelGeonames(@SerializedName("geonames") val list: List<Geonames>)
