package com.example.jesus.tempo.app.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.example.jesus.tempo.R
import com.example.jesus.tempo.app.global.State
import com.example.jesus.tempo.app.model.helper.doReplace
import com.example.jesus.tempo.app.model.helper.getAllPreferences

class HistoricFragment : Fragment() {

    val listView = lazy { view?.findViewById<ListView>(R.id.listaHistorica) }
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_historic, container, false)
    }

    override fun onResume() {
        super.onResume()
        val listViewValue = listView.value
        listViewValue.apply {
            this?.adapter = ArrayAdapter(activity, android.R.layout.simple_list_item_1, getAllPreferences(activity))

        }

        listViewValue?.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                val textView = (p1 as TextView)
                doReplace(DetailFragment(),activity)
                State.observableQuery.onNext(textView.text.toString())
            }
        }
    }
}
