package com.example.jesus.tempo.api.model.facade.accions

import com.example.jesus.tempo.api.model.dao.RetrofitGeonamesDao
import com.example.jesus.tempo.api.model.vo.ModelGeonames


object FetchGeopointsAccion{
    operator fun invoke(query : String) =
            RetrofitGeonamesDao().getElement(query)
}
