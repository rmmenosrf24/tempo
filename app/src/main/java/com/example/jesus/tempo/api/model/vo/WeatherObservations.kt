package com.example.jesus.tempo.api.model.vo

import com.google.gson.annotations.SerializedName


data class WeatherObservations(@SerializedName("lng") val lng: String, @SerializedName("lat")val lat: String, @SerializedName("clouds")val clouds: String,
                               @SerializedName("datetime") val dateTime : String,@SerializedName("temperature") val temperature : String?,
                               @SerializedName("humidity") val humidity: String?, @SerializedName("stationName") val stationName : String,
                               @SerializedName("windSpeed") val windSpeed: String?)
data class ModelWeatherObservations(@SerializedName("weatherObservations") val list: List<WeatherObservations>)