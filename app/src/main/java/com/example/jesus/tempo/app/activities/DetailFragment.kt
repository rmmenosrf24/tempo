package com.example.jesus.tempo.app.activities


import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import com.example.jesus.tempo.R
import com.example.jesus.tempo.api.model.facade.GeoWeatherFacade
import com.example.jesus.tempo.api.model.vo.Coordenadas
import com.example.jesus.tempo.app.global.State
import com.example.jesus.tempo.app.model.helper.savePreference
import com.example.jesus.tempo.app.model.helper.setUpMap
import com.example.jesus.tempo.app.model.helper.showError
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DetailFragment : Fragment() {
    val txtNomeCidade = lazy { view?.findViewById<TextView>(R.id.txtNomeCidade) }
    val txtInfoAdicional = lazy { view?.findViewById<TextView>(R.id.txtInfoAdicional) }
    val map = lazy { childFragmentManager.findFragmentById(R.id.mapView) }
    val bar = lazy { view?.findViewById<BottomNavigationView>(R.id.bnvTemperatura) }
    val context = lazy { getContext() }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onResume() {
        super.onResume()
        State.observableQuery.subscribe({ query ->
            GeoWeatherFacade
                    .fetchGeopoints(query)
                    .flatMap { geopoint ->
                        Log.i("chamada_servizo", geopoint.raw().toString())
                        val geo = geopoint.body()?.list?.first()
                        val (n, e, w, s) = geo!!.bbox
                        GeoWeatherFacade
                                .fetchWeatherObservations(Coordenadas(n, e, w, s)).map { x -> Triple(geo.name, Pair(geo.lat, geo.lng), x) }

                    }.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe({ y ->
                        val (name, p, weather) = y
                        Log.i("chamada_servizo", weather.raw().toString())
                        val list = weather.body()?.list
                        val windSpeedAvg = list?.map { x ->
                            val wind = x.windSpeed ?: "0"
                            wind.toDouble()
                        }?.average() ?: 0.0
                        val humidityAvg = list?.map { x ->
                            val humi = x.humidity ?: "0"
                            humi.toDouble()
                        }?.average() ?: 0.0
                        val temperature = list?.map { x ->
                            val temp = x.windSpeed ?: "0"
                            temp.toDouble()
                        }?.average() ?: 0.0
                        val barra = bar.value
                        val detalles = Html.fromHtml("<b>Velocidade do vento:</b><br/><i>${String.format("%.2f",(windSpeedAvg * 1.8))} km/h</i><br/><b>Humidade:</b><br/><i>${String.format("%.2f",humidityAvg)} %</i>", Html.FROM_HTML_MODE_LEGACY)
                        val listLatLng = list?.map { w -> Pair(w.stationName, LatLng(w.lat.toDouble(), w.lng.toDouble())) };
                        setUpMap(map.value as SupportMapFragment, p, name, listLatLng)
                        txtNomeCidade.value?.text = name

                        txtInfoAdicional.value?.text = detalles

                        when {
                            temperature < 10 -> barra?.setBackgroundColor(resources.getColor(android.R.color.holo_purple))
                            temperature < 15 && temperature > 10 -> barra?.setBackgroundColor(resources.getColor(android.R.color.holo_orange_light))
                            temperature < 20 && temperature > 15 -> barra?.setBackgroundColor(resources.getColor(android.R.color.holo_red_light))
                            else -> barra?.setBackgroundColor(resources.getColor(android.R.color.holo_red_dark))
                        }
                        barra?.menu?.findItem(R.id.mnuTemp)?.title = "${String.format("%.2f",temperature)}º C"
                        savePreference(context.value, name)
                    }, { e -> Log.e("error","-------------------------> ${e.message}")})
        }, { e -> Log.e("error","-------------------------> ${e.message}")})
    }
}
