package com.example.jesus.tempo

import com.example.jesus.tempo.api.model.facade.GeoWeatherFacade
import com.example.jesus.tempo.app.global.State
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.DisplayName

class ExampleUnitTest {


    @Test
    @DisplayName("Dada unha cidade valida ten que retornar unha lista maior que 0")
    fun listMoreThanOne(){
        val geo = GeoWeatherFacade
                    .fetchGeopoints("Madrid")
                    .map{x -> x.body()?.list}
                    .subscribe({x -> Assertions.assertTrue(x!!.isNotEmpty())},{ x -> println("$x")})
    }

    @Test
    fun rxJavaStringWorks(){
        State.observableQuery.onNext("probas")
        State.observableQuery.subscribe{ println(it)}
    }
}

